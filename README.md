# PKU的数学大广场

本项目受 github 上[某项目](https://github.com/Christ096/PKU_math_plaza)启发而来. 由于 github 的 markdown 渲染不支持数学公式, 而且 github 会被间歇性 dns 污染, 所以我在 gitlab 上创建了这个对应项目.

目前我们采取的策略为通过 [issues](https://gitlab.com/pku-math/plaza/-/issues) 提交自己的问题并解答别人的问题.

注意 issues 所在的网址为 [https://gitlab.com/pku-math/plaza/-/issues](https://gitlab.com/pku-math/plaza/-/issues), 是不是特别好记! 或者可以记一个更短的网址 [https://gitlab.com/pku-math/plaza](https://gitlab.com/pku-math/plaza), 在那里你会看到本说明文档, 点击本说明文档中的 [issues](https://gitlab.com/pku-math/plaza/-/issues) 链接就可以了!

可以参考这个[测试样例](https://gitlab.com/pku-math/plaza/-/issues/1)来进行一个数学问题的问和一个数学问题的答.

提交 issue 时, 建议给题目添加关键词, 如这个问题所在的领域, 以便其他人搜索.

在 issue 的内容中, 可以粘贴图片, 也可以使用[数学公式](https://docs.gitlab.com/ee/user/markdown.html#math).

注意 gitlab 中分隔数学公式和正常文本的方式比较特殊:

行内数学公式:

source | result
:-: | :-:
``$`a^2+b^2=c^2`$`` | $`a^2+b^2=c^2`$

行间数学公式:

source:
~~~md
```math
a^2+b^2=c^2
```
~~~

result:
```math
a^2+b^2=c^2
```

github 上的原项目只支持普通的高数/数分/高代/线代/初等概率论/初等统计学. 这个项目没有这个限制!

如果对此测试项目有任何疑问和建议, 可以发 issue 或 pull request, 或者发邮件给 [pkumath@outlook.com](mailto:pkumath@outlook.com).

测试项目, 非盈利, 仅学术, 大家不要违法乱纪.